if (Meteor.isClient) {

    Session.setDefault('counter', 0);
    Template.messages.helpers({
        counter: function () {
            return Session.get('counter');
        }
    });

    Meteor.startup(function () {
        restServiceCall("http://udem.herokuapp.com/parcial");
    });

    var restServiceCall = function (url) {
        HTTP.call( 'GET',url,{}, function(error,response){
            if ( error ) {
                console.log(error);
            } else {
                var messages=response.data.messages;
                var imagen="";
                for(var i=0; i< messages.length; i++) {
                    var data = messages[i];
                    console.log(data.picture);
                    if(data.picture ===""){
                        imagen ="https://cdn3.iconfinder.com/data/icons/black-easy/512/538474-user_512x512.png";
                    }else{
                        imagen=data.picture;
                    }
                    console.log(data);
                    $('.list-noti').append('' +
                        '<div class="col-md-12">'+
                        '<div class="noti">'+

                        '<div class="mensaje"> ' +
                        '<div>Mensaje: <span>' + data.message + '</span></div> ' +
                        '</div> ' +

                        '<div class="imgnot"> ' +
                        '<img src="'+imagen+'"/> ' +
                        '</div> ' +

                        '</div> ' +
                        '</div>');
                }
            }
        });
    };

    var restServiceCallSearch = function (url,search) {
        HTTP.call( 'GET',url,{}, function(error,response){
            if ( error ) {
                console.log(error);
            } else {
                var messages=response.data.messages;
                var imagen="assets/imgs/no_pic.jpg";
                for(var i=0; i< messages.length; i++) {
                    var data = messages[i];

                    if(data.message_id===search){

                        console.log(data.picture);
                        if(data.picture ===""){
                            imagen ="https://cdn3.iconfinder.com/data/icons/black-easy/512/538474-user_512x512.png";
                        }else{
                            imagen=data.picture;
                        }
                        console.log(data);
                        $('.list-noti').empty();
                        $('.list-noti').append('' +
                            '<div class="col-md-12">'+
                            '<div class="noti">'+

                            '<div class="mensaje"> ' +
                            '<div>Mensaje: <span>' + data.message + '</span></div> ' +
                            '</div> ' +

                            '<div class="imgnot"> ' +
                            '<img src="'+imagen+'"/> ' +
                            '</div> ' +

                            '</div> ' +
                            '</div>');

                    }

                }
            }
        });
    };


    Template.messages.events({
        'click button': function(e){
            e.preventDefault();
            restServiceCallSearch("http://udem.herokuapp.com/parcial",13434658);
        }
    });

}


if (Meteor.isServer) {
    Meteor.startup(function () {
        // code to run on server at startup
    });


}